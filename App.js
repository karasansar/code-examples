import React, {Component} from 'react';  
import {Text, View, FlatList, TouchableOpacity} from 'react-native';  
import BootstrapStyleSheet from 'react-native-bootstrap-styles';

const
  BODY_COLOR = '#000022',
  TEXT_MUTED = '#888888';

// custom constants
const constants = {
  BODY_COLOR, TEXT_MUTED,
};

const classes = {
  title: {
    fontSize:85,
  }
};


const bootstrapStyleSheet = new BootstrapStyleSheet(constants, classes);
const s = bootstrapStyleSheet.create();
const c = bootstrapStyleSheet.constants;

export default class App extends Component {  

constructor(props) {
  super(props);
  this.state = {
    loading: true,
    dataSource:[]
   };
 }
 componentDidMount() {
  fetch("https://jsonplaceholder.typicode.com/users")
  .then(response => response.json())
  .then((responseJson)=> {
    this.setState({
     loading: false,
     dataSource: responseJson
    })
  })
  .catch(error=>console.log(error)) //to catch the errors if any
  }
  renderItem=(data)=>
  <TouchableOpacity>
        <Text style={[s.textCenter, {fontSize: 30}]} >{data.item.name}</Text>
        <Text style={[s.textCenter]}>{data.item.email}</Text>
        <Text style={[s.textCenter]}>{data.item.company.name}</Text></TouchableOpacity>
  render() {  
    return (  
      <View style={[ {backgroundColor: `${'grey'}`}]}>
        <View style={[s.container, s.h100, s.justifyContentCenter]}>
          <Text style={[ s.textCenter, s.textDark, s.myMd3, {fontSize: 50}, {marginTop: -80}]}></Text>
          <FlatList style={[{marginTop: 150}]}
         data = {this.state.dataSource}
         ItemSeparatorComponent = {this.FlatListItemSeparator}
         renderItem= {item=> this.renderItem(item)}
         keyExtractor= {item=>item.id.toString()}
        />
        </View>
      </View>
    );  
  }  
}  